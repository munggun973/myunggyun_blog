package com.example.demo.controller;

import java.net.http.HttpRequest;
import java.util.List;
import java.util.function.Supplier;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import com.example.demo.model.RoleType;
import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import net.bytebuddy.TypeCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import com.example.demo.dto.Member;

//html문서를 반환
//@Controller

//데이터를 반환
@RestController
public class HttpRestController {

    @Autowired // DI, 의존성주입
    private UserRepository userRepository;

	private static final String TAG = "HttpControllerTest: ";

	//get test
	//인터넷 브라우저요청은 무조건 get요청밖에 없다.
    @GetMapping("/http/get")
    public String getTest(Member m) {
        System.out.println(TAG + "getter : "+m.getId());
        m.setId(666);
    	return "get 요청 : " + "id: " +  m.getId() + " pw: " + m.getPassword();
    }
    
    //json으로 데이터를 받으면 동일한 파라미터를 가진 vo객체와 매칭시킬 수 있다.
    @PostMapping("/http/post")
    public String postTest() {
        return "post 요청";
    }
    
    @PutMapping("/http/put")
    public String putTest() {
        return "put 요청";
    }
    
    @DeleteMapping("/http/delete")
    public String deleteTest() {
        return "delete 요청";
    }

    //insert
    @PostMapping("/dummy/join")
    public String join(User user) {
        System.out.println("username: " + user.getUsername());
        System.out.println("password: " + user.getPassword());
        System.out.println("email: " + user.getEmail());

        user.setRole(RoleType.USER);
        userRepository.save(user);
        return "회원가입이 완료되었습니다.";
    }

    //select one
    @GetMapping("/dummy/user/{id}")
    public User detail(@PathVariable int id){
        //그냥 findById만으로 user객체를 생성하게 되면 null값이 발생할 수도 있으므로 orEleseGet, orElseThrow를 사용해야한다.
//        User user = userRepository.findById(id).orElseThrow(new Supplier<IllegalArgumentException>() {
//            @Override
//            public IllegalArgumentException get() {
//                return new IllegalArgumentException("해당 유저는 없습니다. id: " + id);
//            }
//        });

        //위 코드를 람다식으로 변경
        User user = userRepository.findById(id).orElseThrow(()->{
            return new IllegalArgumentException("해당 유저는 없습니다. id: " + id);
        });

        //user 객체 = 자바 오브젝트
        //원래는 변환(웹브라우저가 이해할 수 있는 데이터) -> json(Gson라이브러리)
        //스프링부트는 MessageConverter라는 해가 응답시에 자동으로 작동
        //만약에 자바 오브젝트를 리턴하게 되면 MessageConverter가 Jackson을 호출해서 json으로 변환
        return user;
    }

    //select all
    @GetMapping("/dummy/users")
    public List<User> list() {

        return userRepository.findAll();
    }

    //select paging
    @GetMapping("/dummy/user/page")
    public List<User> pageList(@PageableDefault(size = 2, sort = "id", direction = Sort.Direction.DESC)Pageable pageable){
        Page<User> pagingUsers = userRepository.findAll(pageable);

        List<User> users = pagingUsers.getContent();
        return users;
    }

    //update
    @Transactional // 함수 종료시에 자동으로 commit
    @PutMapping("/dummy/user/{id}")
    public User updateUser(@PathVariable int id, @RequestBody User requestUser) { // json 데이터를 요청 -> Java Object(MessageConverter의 Jackson라이브러리로 변환해서 받아준다
        System.out.println("id : " + id);
        System.out.println("password : " + requestUser.getPassword());
        System.out.println("email : " + requestUser.getEmail());

        User user = userRepository.findById(id).orElseThrow(() -> {
           return new IllegalArgumentException("수정에 실패하였습니다.");
        });

        user.setPassword(requestUser.getPassword());
        user.setEmail(requestUser.getEmail());

        //save함수는 id를 전달하지 않으면 insert를 해주고
        //save함수는 id를 전달하면 해당 id에 대한 데이터가 있으면 update를 해주고
        //save함수는 id를 전달하면 해당 id에 대한 데이터가 없으면 insert를 해준다
//        userRepository.save(user);


        //더티 체킹
        //더티 체킹이란 영속성 컨텍스트 안의 user오브젝트에 변경된 데이터를 확인한다는 뜻
        //변경된 데이터가 있다면 update문이 자동으로 실행
        return user;
    }

    //delete
    @DeleteMapping("/dummy/user/{id}")
    public String delete(@PathVariable int id){
        try{
            userRepository.deleteById(id);
        } catch(EmptyResultDataAccessException e) {
            return "삭제할 ID가 존재하지 않습니다.";
        }
        return "삭제되었습니다. id : " + id;
    }
}
