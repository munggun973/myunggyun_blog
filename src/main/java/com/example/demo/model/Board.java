package com.example.demo.model;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Board {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(nullable = false, length = 100)
	private String title;
	
	@Lob // 대용량 데이터
	private String content; // 섬머노트 라이브러리 <html>태그와 섞여서 디자인이됨
	
	@ColumnDefault("0")
	private int count; // 조회수
	
	@ManyToOne // Many = Board, User = One
	@JoinColumn(name="userId")
	private User user; // 원래 DB는 오브젝트를 저장할 수 없기 때문에 foreign key를 사용

	@OneToMany(mappedBy = "board", fetch = FetchType.EAGER) // 연관관계의 주인이 아니다. (난 FK가 아니예요) DB에 컬럼 만들지 마세요!
	private List<Reply> reply; // Board에 foreign key가 있기 때문에 @JoinColumn이 필요가 없다

	@CreationTimestamp
	private Timestamp createDate;
	
}
