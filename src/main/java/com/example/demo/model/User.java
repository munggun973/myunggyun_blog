package com.example.demo.model;

import java.sql.Timestamp;

import javax.persistence.*;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;

//ORM이란? -> Java Object -> 테이블로 만들어주는 기술

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
//@DynamicInsert //insert시 null인 필드를 제외시켜준다
public class User {
	
	@Id // primary key
	@GeneratedValue(strategy=GenerationType.IDENTITY) // 프로젝트에서 연결된 DB의 넘버링 전략을 따라간다.
	private int id; // 시퀀스, auto_increment
	
	@Column(nullable=false, length=30)
	private String username; // 아이디
	
	@Column(nullable=false, length=100)
	private String password;
	
	@Column(nullable=false, length=50)
	private String email;
	
	//@ColumnDefault("'user'") // Enum도입전 코드
	@Enumerated(EnumType.STRING)
	private RoleType role; // Enum을 쓰는게 좋다.(admin, user, manager)
	
	@CreationTimestamp // 시간이 자동으로 입력
	private Timestamp createDate;
	
}
